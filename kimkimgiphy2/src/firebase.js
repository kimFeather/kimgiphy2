import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

var config = {
    apiKey: "AIzaSyAvgSaMHqXAfLQwjtsNA6uW3kWxhDhNos8",
    authDomain: "kim-react-workshop.firebaseapp.com",
    databaseURL: "https://kim-react-workshop.firebaseio.com",
    projectId: "kim-react-workshop",
    storageBucket: "kim-react-workshop.appspot.com",
    messagingSenderId: "82273816348"
};
firebase.initializeApp(config);

const database = firebase.database()
const auth = firebase.auth()
const provider = new firebase.auth.FacebookAuthProvider()

export {
    database,
    auth,
    provider
}