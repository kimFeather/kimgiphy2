import React from 'react';
import ItemGiphy from './ItemGiphy';
import { List } from 'antd';

function ListGiphy(props) {
  console.log('items ', props.items);

  return (
    <List
    pagination={{
      
      pageSize: 40,
    }}
    //   grid={{ gutter: 16, column: 4 }}
      dataSource={props.items}
      grid={{ gutter: 16, column: 4 }}
      renderItem={item => (
        <List.Item>
          <ItemGiphy item={item} onItemGifClick={props.onItemGifClick} />
        </List.Item>
      )}
    />
  );
}

export default ListGiphy;