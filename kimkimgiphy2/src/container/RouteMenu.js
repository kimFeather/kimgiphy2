import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ListFavorite from '../component/favorite/list';
import ListGiphy from '../component/ListGiphy' 

// import Profile from '../Components/profile/index';

function RouteMenu(props) {
    return (
      <Switch>
        <Route
          path="/feed"
          exact
          render={() => {
            return <ListGiphy items={props.items} />;
          }}
        />
        <Route path="/favorite" exact  component={ListFavorite}/>
        {/* <Redirect from="/*" exact to="/" /> */}
      </Switch>
    );


}
export default RouteMenu;