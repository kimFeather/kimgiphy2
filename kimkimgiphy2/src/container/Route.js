import React from 'react';
import { Route, Switch } from 'react-router-dom';
import LoginPage from './LoginPage';
import MainPage from './Main';
import RegisterPage from './RegisterPage';


function Routes() {
    return (
        <div style={{ width: '100%' }}>
            <Switch>

                <Route 
                exact path="/" 
                component={LoginPage} />
                 <Route exact path="/register" component={RegisterPage} />
                <Route 
                //  exact path="/feed" 
                component={MainPage} />
            </Switch>
        </div>
    )
}

export default Routes;